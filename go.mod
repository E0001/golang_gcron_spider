module data

go 1.14

require (
	github.com/360EntSecGroup-Skylar/excelize/v2 v2.2.0
	github.com/gogf/gf v1.13.3
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)
